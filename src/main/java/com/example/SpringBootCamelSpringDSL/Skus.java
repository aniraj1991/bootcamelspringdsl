package com.example.SpringBootCamelSpringDSL;

public class Skus {
	String skuId;
	String productId;
	String color;
	String size;
	
	public Skus()
	{
		
	}
	public Skus(String skuId, String productId, String color, String size) {
		super();
		this.skuId = skuId;
		this.productId = productId;
		this.color = color;
		this.size = size;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}

}
