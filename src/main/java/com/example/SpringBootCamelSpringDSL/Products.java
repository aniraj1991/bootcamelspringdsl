package com.example.SpringBootCamelSpringDSL;


public class Products {
	Long ProductId;
	String ProductName;
	String ProductImage;
	
	public Products()
	{
		
	}
	public Products(Long productId, String productName, String productImage) {
		super();
		ProductId = productId;
		ProductName = productName;
		ProductImage = productImage;
	}
	
	
	public Long getProductId() {
		return ProductId;
	}
	
	
	public void setProductId(Long productId) {
		ProductId = productId;
	}
	
	public String getProductName() {
		return ProductName;
	}
	
	public void setProductName(String productName) {
		ProductName = productName;
	}
	
	public String getProductImage() {
		return ProductImage;
	}
	
	public void setProductImage(String productImage) {
		ProductImage = productImage;
	}

}
