package com.example.SpringBootCamelSpringDSL;

public class ProductDetails {
	Products prds;
	Skus skus;
	public ProductDetails(Products prds, Skus skus) {
		super();
		this.prds = prds;
		this.skus = skus;
	}
	public Products getPrds() {
		return prds;
	}
	public void setPrds(Products prds) {
		this.prds = prds;
	}
	public Skus getSkus() {
		return skus;
	}
	public void setSkus(Skus skus) {
		this.skus = skus;
	}
}
