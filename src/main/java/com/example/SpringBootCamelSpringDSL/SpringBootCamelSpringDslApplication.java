package com.example.SpringBootCamelSpringDSL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath:routes.xml"})
public class SpringBootCamelSpringDslApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCamelSpringDslApplication.class, args);
	}

}

