package com.example.SpringBootCamelSpringDSL;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;


public class CustomAggregationStrategy implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange)
	{
		// TODO Auto-generated method stub
		if (oldExchange == null && newExchange != null)
		{
			oldExchange = newExchange;
		}
		if(newExchange.getIn().getBody() instanceof Products)
		{
			newExchange.setProperty("prodresp", newExchange.getIn().getBody());
		}
		else if(newExchange.getIn().getBody() instanceof Skus)
		{
			newExchange.setProperty("skuresp", newExchange.getIn().getBody());
		}
		
		if(oldExchange.getProperty("prodresp") != null)
		{
			newExchange.setProperty("prodresp", oldExchange.getProperty("prodresp"));
		}
		if(oldExchange.getProperty("skuresp") != null)
		{
			newExchange.setProperty("skuresp", oldExchange.getProperty("skuresp"));
		}
		return newExchange;
	}

}
