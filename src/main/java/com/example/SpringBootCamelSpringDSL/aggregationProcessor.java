package com.example.SpringBootCamelSpringDSL;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class aggregationProcessor implements Processor  {
	@Override
	public void process(Exchange exchange) throws Exception {
		Products prd = (Products) exchange.getProperty("prodresp");
		Skus sku = (Skus) exchange.getProperty("skuresp");
		ProductDetails dtl = new ProductDetails(prd,sku);
		exchange.getIn().setBody(dtl);
	}
}
